import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import Button from '../index';
import User from '../user';
import UserProfile from '../user_list';

storiesOf('User', module)
  .add('default view', () => {
    return (
      <User />
    )})
  .add('some other view', () => {
    return (
      <User fName='Elton' lName='John' age='55' photo= 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTubE1KGwXwXBzk_L10-RPt1Yqg7qjj7i08VgWEpKb31oiQsYv4QQ'/>
    )})
  .add('list view', (elt) => {
    return (
      <User fName={ elt.fName } lName={ elt.lName } age={ elt.age } photo={ elt.photo }/>
    )});

storiesOf('UserProfile', module)
.add('default view', () => {
  return (
    <UserProfile />
  );
});
