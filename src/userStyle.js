export const frameStyle = {
  backgroundColor: 'pink',
  maxWidth:'280px',
  minWidth: '240px',
  textAlign:'center',
  padding: '20px 0px',
};
export const photoStyle = {
  border: '2px solid purple',
  padding: '3px 10px',
  width: '200px'
};
export const txtDivStyle = {
  border: '2px solid purple',
  padding: '3px 10px',
  color: 'navy',
  fontSize: '20px',
  width:'200px',
  margin: 'auto',
  marginTop: '3px'
}
