import React from 'react';

const ProfilePhotoComponent = ({ imgPath, changePhoto }) => (
	<div>
	  <div >
		  <img src={imgPath} onClick={() => {
		  	window.open(imgPath);
		  }} />
	  </div>
	  <div >
	    <button onClick={changePhoto}>Edit photo</button>
	  </div>
	</div>
);

export default ProfilePhotoComponent;