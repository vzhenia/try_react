import React, { Component } from 'react';
import ProfileComponent from './user';

import UserList from './listOfUsers.js';

const userKeys = Object.keys(UserList);

const UserProfile = () => (
  <div>
    {userKeys.map((key) => (<ProfileComponent {...UserList[key]} />))}
  </div>
); 

export default UserProfile;
