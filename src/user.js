import React, { PropTypes } from 'react';
import ProfilePhoto from './ProfilePhotoComponent';
console.log(ProfilePhoto)

import { frameStyle, photoStyle, txtDivStyle } from './userStyle.js';

class User extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ...props
    }

    this.changePhoto = this.changePhoto.bind(this);
  }

  changePhoto() {
    const imgUrl = prompt('set photo', '');
    imgUrl && this.setState({'photo': imgUrl});
  }

  render() {
    const {fName, lName, age} = this.props;

    return (
      <div style={frameStyle}>
        <ProfilePhoto imgPath={this.state.photo} changePhoto={this.changePhoto} />
        <div style={txtDivStyle}>Last name: {fName}</div>
        <div style={txtDivStyle}>First name: {lName}</div>
        <div style={txtDivStyle}>Age: {age}</div>
        <button onClick={() => {
          console.log(this.state)
        }}>
          send all data
        </button>
      </div>
    );
  }
}

User.defaultProps = {
  fName: 'Coco',
  lName: 'Chanel',
  age: 45,
  photo: 'http://www.queness.com/resources/images/png/apple_ex.png'
};

User.propTypes = {
  fName: PropTypes.string,
  lName: PropTypes.string,
  age: PropTypes.number
};

export default User;
